package include;

public class CmdLineBuilder {
	
	private StringBuilder args = new StringBuilder();
	
	public CmdLineBuilder addToken(String token) {
		args.append(token);
		args.append('\0');
		return this;
	}
	
	public String getArgs() {
		return args.toString();
	}
	
	
}
