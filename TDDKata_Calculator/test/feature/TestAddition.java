package feature;

import include.CmdLineBuilder;
import main.DefaultScalc;
import main.ScalcRunner;
import main.ScalcView;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class TestAddition {
	
	private DefaultScalc scalc;
	private ScalcView view;
	
	@Before
	public void beforeTest() {
		scalc = new DefaultScalc();
		view = mock(ScalcView.class);
	}
	
	
	@Test
	public void returnsSumOfLongSequenceSeparatedWithComma() {
		
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd = cmd.addToken("scalc");
		cmd = cmd.addToken("1,2,3,4,5,6,7,8,9,0");
		
		runner.run(cmd.getArgs());

		verify(view).display("45");
	}
	
	@Test
	public void returnsSumOfLongSequenceSeparatedWithCommasAndEOLs() {
		
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd = cmd.addToken("scalc");
		cmd = cmd.addToken("2\n4,6\n8,10\n12,14");
		
		runner.run(cmd.getArgs());

		verify(view).display("56");
	}
	
	@Test
	public void returnsSumOfLongSequenceSeparatedWithCustomDelimiter() {
		
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd = cmd.addToken("scalc");
		cmd = cmd.addToken("//;\n1;3;5;7;9;11;13");
		
		runner.run(cmd.getArgs());

		verify(view).display("49");
	}
	
	@Test
	public void returnsSumOfLongSequenceSeparatedWithCustomLongDelimiter() {
		
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd = cmd.addToken("scalc");
		cmd = cmd.addToken("//[===]\n2===4===8===16");
		
		runner.run(cmd.getArgs());

		verify(view).display("30");
	}
	
	@Test
	public void returnsSumOfLongSequenceSeparatedWithMultipleCustomDelimiters() {
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd = cmd.addToken("scalc");
		cmd = cmd.addToken("//[***][||][%]\n1%3||45***5%1");
		
		runner.run(cmd.getArgs());

		verify(view).display("55");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void dispaysErrorMessageIfThereAreNegativeArgs() {
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd = cmd.addToken("scalc");
		cmd = cmd.addToken("1,2,3,4,-5,6,7,8,9");
		
		runner.run(cmd.getArgs());
	}
	
	@Test
	public void returnsSumOfSequenceIgnoringBigValues() {
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd = cmd.addToken("scalc");
		cmd = cmd.addToken("1024,1,1025,3,1024,5");
		
		runner.run(cmd.getArgs());

		verify(view).display("9");
	}
	
}
