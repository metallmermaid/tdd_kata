package unit;

import include.CmdLineBuilder;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestCmdLineBuilder {

	@Test
	public void storesAndReturnsAllTheArgsCorrectly() {
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		String appName = "app_name", 
				arg1 = "--arg1 value1", 
				arg2 = "--arg2=value2";
		
		cmd = cmd.addToken(appName);
		cmd = cmd.addToken(arg1);
		cmd = cmd.addToken(arg2);
		
		String[] args = cmd.getArgs().split("\0");
		assertEquals(appName, args[0]);
		assertEquals(arg1, args[1]);
		assertEquals(arg2, args[2]);
		
	}
}

