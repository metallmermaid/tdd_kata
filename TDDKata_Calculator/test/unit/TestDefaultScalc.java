package unit;

import main.DefaultScalc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestDefaultScalc {
	
	private DefaultScalc sc;
	
	@Before
	public void beforeTest() {
		sc = new DefaultScalc();
	} 
	
	@Test
	public void returnsZeroOnEmptyInput() {
		long result = sc.add("");
		assertEquals(0, result);
	}
	
	@Test
	public void returnsArgValueIfThereIsOnlyOneArg() {
		long result = sc.add("24");
		assertEquals(24, result);
	}
	
	@Test
	public void returnsSumOfCommaSeparatedArgs() {
		long result = sc.add("2,2");
		assertEquals(4, result);
	}
	
	@Test
	public void returnsSumOfLongCommaSeparatedSequence() {
		long result = sc.add("0,1,1,2,3,5,8,13,21");
		assertEquals(54, result);
	}
	
	@Test
	public void returnsSumOfSequenceWithCustomDelimiter() {
		long result = sc.add("//:\n3:3:3");
		assertEquals(9, result);
	}
	
	@Test
	public void returnsSumOfSequenceWithCustomLongDelimiter() {
		long result = sc.add("//[***]\n1***6***13");
		assertEquals(20, result);
	}
	
	@Test
	public void returnsSumOfSequenceWithMultipleCustomDelimiters() {
		long result = sc.add("//[||][%]\n1||2%3||4");
		assertEquals(10, result);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void throwsOnNegativeArgs() {
		sc.add("10,20,-30,40");
	}
	
	@Test
	public void returnsSumOfSequenceIgnoringBigInts() {
		long result = sc.add("//#\n1#1001#2#1002#3");
		assertEquals(6, result);
	}
}
