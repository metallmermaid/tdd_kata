package unit;

import include.CmdLineBuilder;
import main.Scalc;
import main.ScalcRunner;
import main.ScalcView;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class TestScalcRunner {
	
	private Scalc scalc;
	private ScalcView view;
	
	@Before
	public void beforeTest() {
		scalc = mock(Scalc.class);
		view = mock(ScalcView.class);
	}
	
	@Test
	public void transmitsCorrectlyRarsedToSCalc() {
		
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd.addToken("scalc");
		cmd.addToken("2,4,6,8,10");
		
		runner.run(cmd.getArgs());
		
		verify(scalc).add(cmd.getArgs());
	}
	
	@Test
	public void transmitsSCalcResultToDisplay() {
		
		ScalcRunner runner = new ScalcRunner(scalc, view);
		CmdLineBuilder cmd = new CmdLineBuilder();
		
		cmd.addToken("scalc");
		cmd.addToken("");
		
		when(scalc.add(cmd.getArgs())).thenReturn(Long.valueOf(666));
		runner.run(cmd.getArgs());
		verify(view).display("666");
		
	}
	
}
