package main;

public class ScalcRunner {
	
	private Scalc scalc;
	private ScalcView view;
	
	public ScalcRunner(Scalc scalc, ScalcView view) {
		this.scalc = scalc;
		this.view = view;
	}
	
	public void run(String args) {
		long result = scalc.add(args);
		view.display(String.valueOf(result));
	}
	
}
