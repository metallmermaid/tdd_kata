package main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DefaultScalc implements Scalc {
	
	@Override
	public long add(String args) {
		if(args.isEmpty()) {
			return 0;
		}
		
		long result = 0;

		for(String token : args.split("\0")) {
			
			String delimiter = "[,\n]";

	        Pattern p = Pattern.compile( "^//(.*)\n(.*)$" );
	        Matcher m = p.matcher( token );
	        if (m.find()) {
	        	delimiter = m.group(1);
	        	token = m.group(2);

	            Pattern p1 = Pattern.compile( "^[^\\d]$" );
	            Matcher m1 = p1.matcher( delimiter );
	            if (m1.find()) {
	            	delimiter = m1.group();
	            } else {
	                Pattern p2 = Pattern.compile( "\\[([^\\]]+)\\]" );
	                Matcher m2 = p2.matcher( delimiter );

	                delimiter = "(";
	                String delim = "";
	                while (m2.find()) {
	                    String d = m2.group(1);
	                    d = d.replaceAll( "\\*", "\\\\*" );
	                    d = d.replaceAll( "\\|", "\\\\|" );

	                    delimiter += delim;
	                    delimiter += d;
	                    delim = "|";
	                }
	                delimiter += ")";
	            }
	        }
	        
			for(String element : token.split(delimiter)) {
				try{
					long number = Long.valueOf(element);
					if(number < 0) {
						throw new IllegalArgumentException("Negative number: " + number);
					}
					if(number < 1000) {
						result += number;
					}
				} catch (NumberFormatException e) {
					//Added for tokens, like "scalc"
					//System.out.println("oops!\n" + e.getMessage());
				}
			}
		}
		
		return result;
	}
	
}
